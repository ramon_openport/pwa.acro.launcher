export interface MediaUpload {
    id: number;
    tagName: string;
    data: string;
}