import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private basic: string = 'Basic b3BlbnBvcnQtY2xpZW50Om9wZW5wb3J0LXNlY3JldA==';
    private currentUserSubject: BehaviorSubject<any>;
    public currentUser: Observable<any>;

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): any {
        return this.currentUserSubject.value;
    }

    login(username: string, password: string) {

        const headers = new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded',
            'App-Code': 'OPT_DEFAULT',
            'Authorization': this.basic });
        const options = { headers: headers };

        let body = `username=${username}&password=${password}&grant_type=password`;

        return this.http.post<any>(`https://uat-api.openport.com/integration-api/authenticate`, body, options)
                    .toPromise()
                    .then(user => {
                        if (user && user.access_token) {
                            // store user details and jwt token in local storage to keep user logged in between page refreshes
                            localStorage.setItem('currentUser', JSON.stringify(user));
                            this.currentUserSubject.next(user);
                        }
                        return user;
                    });
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
}