import { Injectable } from '@angular/core';
import { DexieService } from './dexie.service';
import Dexie from 'dexie';
import { HttpClient, HttpRequest } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class RequestService {

    queueList: Dexie.Table<RequestQueue, Number>;
    failedQeueList: Dexie.Table<RequestQueue, Number>;

    constructor(private dexie: DexieService, 
                private http: HttpClient) {
        this.queueList = dexie.table('requestQueue');
        this.failedQeueList = dexie.table('failedQueue');
    }

    public async queue(request: any): Promise<void> {
        if(!request.retry) request.retry = 0;
        if(!request.status) request.status = 'A';

        const id:Number = await this.queueList.add(request);
        const queue:RequestQueue = await this.remove(id.valueOf());
        await this.processQueue(queue);
    }

    public async pop(): Promise<RequestQueue> {
        const queue: RequestQueue = await this.queueList.where('status').notEqual('E').first();
        if(queue) await this.queueList.delete(queue.id);
        return queue;
    }

    public async peek() : Promise<RequestQueue> {
        const queue: RequestQueue = await this.queueList.where('status').notEqual('E').first();
        return queue;
    }

    public async remove(id: number) : Promise<RequestQueue> {
        const queue: RequestQueue = await this.queueList.where('id').equals(id).first();
        if(queue) this.queueList.delete(queue.id);
        return queue;
    }

    public async count() : Promise<number> {
        return this.queueList.count();
    }

    public async processQueue(queue: RequestQueue) : Promise<void> {
        
        if(!queue) return;
        console.log(queue);
        let response: any;
        if(queue.method == 'GET') response = this.http.get(queue.url, JSON.parse(queue.param)).toPromise();
        if(queue.method == 'POST') response = this.http.post(queue.url, JSON.parse(queue.param)).toPromise();
        
        if(response) {
            await response.then(() => {
                console.log('Successfully submitted event:', queue);
            }).catch(() => {
                console.error('Error in sending event: ', queue);
                let retry: number = queue.retry | 0;
                retry = retry + 1;
                if(retry > 10) queue.status = 'E';
                
                this.failedQeueList.add({
                    url: queue.url,
                    retry: retry,
                    status: queue.status,
                    method: queue.method,
                    param: queue.param,
                    headers: queue.headers
                } as any);
            });
        }else{
            console.log('Unsupported method: ', queue);
        }
    }

    public async process(): Promise<void> {
        for(let i=0; i <= 100; i++)  {
            const queue = await this.pop();
            await this.processQueue(queue);
        }
    }
}