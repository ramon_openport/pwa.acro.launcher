import Dexie from 'dexie';

export class DexieService extends Dexie {

    constructor() {
        super('PWAMobileStorage');

        this.version(2).stores({
            images: '++id,data,tagName',
            documents: '++id,data,tagName',
            requestQueue: '++id,url,method,headers,param,status,retry',
            failedQueue: '++id,url,method,headers,param,status,retry'
        });
    }

}