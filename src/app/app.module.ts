import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
/* import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment'; */
import { DexieService } from 'src/core/service/dexie.service';
import { PwaServiceService } from './pwa-service.service';
import { RequestService } from 'src/core/service/request.service';
import { JwtInterceptor } from 'src/core/util/jwt-http.interceptor';

import { SignaturePadModule } from 'angular2-signaturepad'


import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
  //    .register('./ngsw-worker.js', { enabled: environment.production }),
    HttpClientModule,
    FormsModule,
    SignaturePadModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatButtonModule
  ],
  providers: [
    PwaServiceService,
    RequestService,
    DexieService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
