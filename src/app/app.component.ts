import { Component, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PwaServiceService } from './pwa-service.service';
import { RequestService } from 'src/core/service/request.service';
import { AuthenticationService } from 'src/core/util/authentication.service';
import { MediaUpload } from 'src/core/model/media-upload.model';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  @ViewChild(SignaturePad, null) signaturePad: SignaturePad;

  signaturePadOptions: Object = { // passed through to szimek/signature_pad constructor
    'minWidth': 5,
    'canvasWidth': 400,
    'canvasHeight': 100
  };

  title = 'Mobile-PWA';
  interval: any;
  onprocess = false;
  pingInterval: any;
  requestCount: number = 0;
  images: any[] = [];
  documents: any[] = [];
  captureImage: any;
  captureDocument: any;
  currentDateTime: Date;
  pushNotifId: string;
  username: string;
  password: string;
  worker: any;

  documentTitle: string = 'Documents';
  imageTitle: string = 'Images';

  constructor(private service: PwaServiceService,
              private requestSvc: RequestService,
              private authService: AuthenticationService) {
}

ngAfterViewInit() {
 
}

drawComplete() {
  // will be notified of szimek/signature_pad's onEnd event
  console.log(this.signaturePad.toDataURL());
}

drawStart() {
  // will be notified of szimek/signature_pad's onBegin event
  console.log('begin drawing');
}

  public async ngOnInit() {
    setInterval(() => {
      this.currentDateTime = new Date();
    }, 1000);

    this.interval = setInterval(async () => {
      if(!this.onprocess) {
          this.onprocess = true;
          try {
            await this.requestSvc.process();
            await this.requestSvc.count().then(n => {
              this.requestCount = n;
            });
          } finally {
            this.onprocess = false;
          }
      }
      
    }, 15 * 1000);

    //load cache documents
    const documentsCache:MediaUpload[] = await this.service.documentList();
    for(let o of documentsCache)
      this.documents.push(o.data);
    
    //load cache images
    const imagesCache:MediaUpload[] = await this.service.imageList();
    for(let o of imagesCache)
      this.images.push(o.data);

  

  }

  public ngOnDestroy() {
    console.log("done timeout");
    clearInterval(this.interval);
  }

  public sendPing() : void {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position: Position) => {
        if (position) {
          const lat = position.coords.latitude;
          const lng = position.coords.longitude;
          
          const postPing = this.service.postPing({latitude: lat, longitude: lng, deviceId: this.pushNotifId});
          postPing.then(() => {
            console.log('Sending location');
          });
        }
      },
        (error: PositionError) => {
          console.log(error) 
        });
    }
  }

  public performBackgroundPing() : void {
    if (navigator.geolocation) {
      if(this.pingInterval) {
        clearInterval(this.pingInterval);
        this.pingInterval = null;
      } else {
         this.pingInterval = setInterval(() => {
            this.sendPing();
         }, 1000 * 60 * 15);
      }
    } else {
      alert("Geolocation is not supported by this browser.");
    }
  }

  public onChangeImage(event:any) : void {
    const input = event.target;
    const file = input.files[0];

    var myReader:FileReader = new FileReader();
    this.captureImage = null;

    myReader.onloadend = (e) => {
      this.images.push(myReader.result);
      this.service.uploadImage({
        tagName: file.name,
        data: myReader.result
      }, 'image');
    };

    if(file) myReader.readAsDataURL(file);
  }

  public onChangeDocument(event:any) : void {
    const input = event.target;
    const file = input.files[0];

    var myReader:FileReader = new FileReader();
    this.captureDocument = null;

    myReader.onloadend = (e) => {
      
      let counter: number = 0;
      this.documentTitle = "Analyzing document.";
      this.worker.recognize(myReader.result as any, 'eng')
              .progress(p => {
                if((counter % 5) == 0) this.documentTitle = "Analyzing document.";
                else this.documentTitle = this.documentTitle += ".";
                console.log(p);
                counter ++;
              })
              .then(result => {
                if(result.confidence < 80)
                  alert("Not a document.");
                else {
                  this.documents.push(myReader.result);
                  this.service.uploadImage({
                    tagName: file.name,
                    data: myReader.result
                  }, 'document');
                }

                this.documentTitle = "Documents";
      }).catch(e => {
        alert('Unable to read document. ');
        console.log(e);
        this.documentTitle = "Documents";
      });
      
    };

    if(file) myReader.readAsDataURL(file);
  }

  public async login() : Promise<void> {
    const user = await this.authService.login(this.username, this.password);
  }

  public logout() : void {
    this.authService.logout();
  }

  get isLoggedIn() {
    return this.authService.currentUserValue != null;
  }

  deleteMediaUpload(type: string) {
    this.service.deleteMediaUpload(type);
    if(type === 'document') this.documents.length = 0;
    else this.images.length = 0;
  }
}
